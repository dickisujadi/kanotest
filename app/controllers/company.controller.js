const Company = require('../models/company.model');
const path = require('path');

exports.create = (req, res) => {
    if(!req.body.content) {
        return res.status(400).send({
            message: "company content can not be empty"
        });
    }

    const company = new Company({
        title: req.body.title || "Untitled company", 
        content: req.body.content
    });

    company.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the company."
        });
    });
};

exports.findAll = (req, res, next) => {
    const take = parseInt(req.query.take);
    const skip = parseInt(req.query.skip);

    Company.find()
    .skip(skip)
    .limit(100)
    .then(companies => {
        res.send(companies);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving companies."
        });
    });
};

exports.findOne = (req, res) => {
    Company.findOne({CMGUnmaskedID : req.params.CMGUnmaskedID})
    .then(company => {
        if(!company) {
            return res.status(404).send({
                message: "company not found with id " + req.params.CMGUnmaskedID
            });            
        }
        res.send(company);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "company not found with id " + req.params.CMGUnmaskedID
            });                
        }
        return res.status(500).send({
            message: "Error retrieving company with id " + req.params.CMGUnmaskedID
        });
    });
};

exports.update = (req, res) => {
    if(!req.body.content) {
        return res.status(400).send({
            message: "company content can not be empty"
        });
    }

    Company.findByIdAndUpdate(req.params.CMGUnmaskedID, {
        title: req.body.title || "Untitled company",
        content: req.body.content
    }, {new: true})
    .then(company => {
        if(!company) {
            return res.status(404).send({
                message: "company not found with id " + req.params.CMGUnmaskedID
            });
        }
        res.send(company);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "company not found with id " + req.params.CMGUnmaskedID
            });                
        }
        return res.status(500).send({
            message: "Error updating company with id " + req.params.CMGUnmaskedID
        });
    });
};

exports.delete = (req, res) => {
    Company.findByIdAndRemove(req.params.CMGUnmaskedID)
    .then(company => {
        if(!company) {
            return res.status(404).send({
                message: "company not found with id " + req.params.CMGUnmaskedID
            });
        }
        res.send({message: "company deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "company not found with id " + req.params.CMGUnmaskedID
            });                
        }
        return res.status(500).send({
            message: "Could not delete company with id " + req.params.CMGUnmaskedID
        });
    });
};
