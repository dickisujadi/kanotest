module.exports = (app) => {
    const companies = require('../controllers/company.controller');

    app.post('/companies', companies.create);

    app.get('/companies', companies.findAll);

    app.get('/companies/:CMGUnmaskedID', companies.findOne);

    app.put('/companies/:CMGUnmaskedID', companies.update);

    app.delete('/companies/:CMGUnmaskedID', companies.delete);
}