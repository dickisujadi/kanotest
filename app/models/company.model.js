const mongoose = require('mongoose');

const CompanySchema = mongoose.Schema({
    CMGUnmaskedID: String,
    CMGUnmaskedName: String,
    ClientTier: String,
    GCPStream: String,
    GCPBusiness: String,
    CMGGlobalBU: String,
    CMGSegmentName: String,
    GlobalControlPoint: String,
    GCPGeography: String,
    GlobalRelationshipManagerName: String,
    REVENUE_FY14: Number,
    REVENUE_FY15: String,
    Deposits_EOP_FY14: String,
    Deposits_EOP_FY15x: String,
    TotalLimits_EOP_FY14: String,
    TotalLimits_EOP_FY15: String,
    TotalLimits_EOP_FY15x: String,
    RWAFY15: String,
    RWAFY14: Number,
    ['REV/RWA FY14']: String,
    ['REV/RWA FY15']: Number,
    NPAT_AllocEq_FY14: String,
    NPAT_AllocEq_FY15X: String,
    Company_Avg_Activity_FY14: String,
    Company_Avg_Activity_FY15: String,
    ROE_FY14: String,
    ROE_FY15: String
},{
    timestamps: true
});

module.exports = mongoose.model('Company', CompanySchema);